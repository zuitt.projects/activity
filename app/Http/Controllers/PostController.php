<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use Illuminate\Support\Facades\Route;

class PostController extends Controller
{

  public function welcome()
    {
        //retrieved 3 randomized posts
        $posts = Post::inRandomOrder()->where('isActive', '!=' , '0')->limit(3)
            ->get();

        return view('welcome')->with('posts', $posts);
    }

  public function create()
  {
    return view('posts.create');
  }

  //let's create post controler to store the data to the database
  public function store(Request $request)
  {
      //if there is an authenticated user
    if(Auth::user()){
        //create a new Post objet from the Post model
        $post = new Post;

        //define the properties of the $post object using received from
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        //get the id of the authenticated user and set it as the value of the user_id column
        $post->user_id = (Auth::user()->id);
        //save the post object to the database
        $post->save();

        return redirect('/posts');
    }else{
        return redirect('/login');
    }
  }

  //let's create method for index post from routes
public function index()
    {
        //get all posts from the database
        //Post::where('type', $type)->where('restrict', '!=' , 'res1')
        //$posts = Post::all();
        $posts = Post::all()->where('isActive', '!=' , '0');
        return view('posts.index')->with('posts', $posts);
    }

    public function myPosts()
    {
      //if the user login
      if(Auth::user()){
        $posts = Auth::user()->posts; //retrieve user's own post

        return view('posts.index')->with('posts', $posts);
      }else{
        return redirect('/login');
      }
    }

    //editing specific post upon click
    public function show($id)
    {
      $post = Post::find($id);
      return view('posts.show')->with('post', $post);
    }

    public function edit($id)
    {
        $post = Post::find($id);
        return view('posts.edit')->with('post', $post);        
    }

    public function update(Request $request, $id)//pass both the form data in the request as well as the id of the post to be updated
    {
      // if(Auth::user()){
      //   //update the post id
      //   $post = Post::find($id);

      //   //define the properties of the $post object using received from
      //   $post->title = $request->input('title');
      //   $post->content = $request->input('content');
      //   //get the id of the authenticated user and set it as the value of the user_id column
      //   $post->user_id = (Auth::user()->id);
      //   //save the post object to the database
      //   $post->update();

      //   return view('posts.show')->with('post', $post);
      // }

      $post = Post::find($id);
      //if authenticated user's id is the same as the post's user_id
      if(Auth::user()->id == $post->user_id){
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->save();

       // return view('posts.show')->with('post', $post);
        return redirect('/posts')->with('message', 'Post Updated Successfully');

      }
    }

    //delete post

    // public function destroy($id){

    //   $post = Post::find($id);
    //   if(Auth::user()->id == $post->user_id){
    //     $post->delete();
    //   }
    //   return redirect('/posts')->with('message', 'Post Deleted Successfully');
    // }

    //archive

    public function archive($id){

      $post = Post::find($id);
      if(Auth::user()->id == $post->user_id){
        $post->isActive = '0';
        $post->save();

      }
      return redirect('/posts')->with('message', 'Post Archive Successfully');
    }
    
}

//means need natin gumawa ng posts folder then create na file
